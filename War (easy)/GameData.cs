﻿using CardLibrary;
using LanguageExt;

namespace War__easy_
{
    public interface PlayerCard
    {
        Card card { get; }
    }
    public record class Player1Card(Card card) : PlayerCard;
    public record class Player2Card(Card card) : PlayerCard;

    public interface PlayerDeck<T> where T : PlayerCard
    {
        Seq<T> deck { get; }
    }
    public record class Player1Deck(Seq<Player1Card> deck) : PlayerDeck<Player1Card>;
    public record class Player2Deck(Seq<Player2Card> deck) : PlayerDeck<Player2Card>;

    public record class GamePreparedData(Trump trump, Player1Deck player1, Player2Deck player2);

    public record class GameResultRoundData(Player1Card player1, Player2Card player2, Winner winner);

    public record class Player1ScorePile(Seq<PlayerCard> deck) : PlayerDeck<PlayerCard>;
    public record class Player2ScorePile(Seq<PlayerCard> deck) : PlayerDeck<PlayerCard>;
    public record class GameResultScoreData(Player1ScorePile player1, Player2ScorePile player2, Winner winner);

    public record class GameResultData(Trump trump, Seq<GameResultRoundData> roundDatas, GameResultScoreData scoreData);
}
