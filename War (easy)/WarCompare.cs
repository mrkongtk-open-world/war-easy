﻿using CardLibrary;

namespace War__easy_
{
    internal static class RankExtension
    {
        public static int toInt(this Rank rank)
        {
            return rank switch
            {
                Rank.Two => 2,
                Rank.Three => 3,
                Rank.Four => 4,
                Rank.Five => 5,
                Rank.Six => 6,
                Rank.Seven => 7,
                Rank.Eight => 8,
                Rank.Nine => 9,
                Rank.Ten => 10,
                Rank.Jack => 11,
                Rank.Queen => 12,
                Rank.King => 13,
                Rank.Ace => 14,
                _ => throw new NotImplementedException(),
            };
        }
    }

    public record class TrumpSuiteCompare(Trump trump) : ICompare<Suite>
    {
        public CompareResult Compare(Suite a, Suite b)
        {
            if (a == b)
            {
                return CompareResult.Equals;
            }
            if (a == trump.suite)
            {
                return CompareResult.Left;
            }
            if (b == trump.suite)
            {
                return CompareResult.Right;
            }
            return CompareResult.Equals;
        }
    }

    public record class RankCompare : ICompare<Rank>
    {
        public CompareResult Compare(Rank a, Rank b)
        {
            return a.toInt().CompareTo(b.toInt()) switch
            {
                < 0 => CompareResult.Right,
                > 0 => CompareResult.Left,
                _ => CompareResult.Equals,
            } ;
        }
    }


    public record class WarCompare(Trump trump) : ICompare<PlayerCard>
    {
        private readonly TrumpSuiteCompare trumpSuiteComparer = new TrumpSuiteCompare(trump);
        private readonly RankCompare rankComparer = new RankCompare();

        public CompareResult Compare(PlayerCard a, PlayerCard b)
        {
            var trumpSuiteCompareResult = trumpSuiteComparer.Compare(a.card.suite, b.card.suite);
            return trumpSuiteCompareResult switch
            {
                CompareResult.Equals => rankComparer.Compare(a.card.rank, b.card.rank),
                _ => trumpSuiteCompareResult
            };
        }
    }
}

