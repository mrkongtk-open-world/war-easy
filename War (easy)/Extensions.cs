﻿using CardLibrary;
using LanguageExt;
using LanguageExt.Common;
using static LanguageExt.Prelude;

namespace War__easy_
{
    public static class Extensions
    {
        public static Either<Error, T> GetRandom<T>(this Arr<T> source, Random random)
        {
            // check for empty list
            if (source.Length() <= 0)
            {
                return Error.New("array cannot be empty");
            }
            return source[random.Next(source.Length())];
        }

        public static Suite ToNonOption(this Option<Suite> suite, Suite ifNull)
        {
            return suite.IfNone(() => ifNull);
        }

        public static Either<Error, (Seq<T>, Seq<T>)> EvenSplit<T>(this IEnumerable<T> source)
        {
            if (source.Length() % 2 != 0)
            {
                return Error.New($"cannot split evently with size: {source.Length()}");
            }
            var left = Seq<T>();
            var right = Seq<T>();
            using (var enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    left = left.Add(enumerator.Current);
                    enumerator.MoveNext();
                    right = right.Add(enumerator.Current);
                }
            }
            var result = (left, right);
            return result;
        }

        public static Winner toWinner(this CompareResult result)
        {
            return result switch
            {
                CompareResult.Left => Winner.Player1,
                CompareResult.Right => Winner.Player2,
                _ => Winner.Tie,
            };
        }

        public static string Display(this Rank rank)
        {
            return rank.ToString();
        }

        public static string Display(this Suite suite)
        {
            return suite.ToString();
        }

        public static string Display(this Card card)
        {
            return $"Card({card.suite.Display()},{card.rank.Display()})";
        }

        public static string Display(this Trump trump)
        {
            return $"Trump({trump.suite.Display()})";
        }

        public static string Display(this Winner winner)
        {
            return winner.ToString();
        }

        public static string Display(this GameResultRoundData data)
        {
            return string.Join("\t", Seq(
                    $"Player1: {data.player1.card.Display()}",
                    $"Player2: {data.player2.card.Display()}",
                    $"Round winner: {data.winner.Display()}"
                    ));
        }

        public static string Display(this GameResultScoreData data)
        {
            return string.Join("\t", Seq(
                    $"Player1 score: {data.player1.deck.Count}",
                    $"Player2 score: {data.player2.deck.Count}",
                    $"Game winner: {data.winner.Display()}"
                    ));
        }
    }

}

