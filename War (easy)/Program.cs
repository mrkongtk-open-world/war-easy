﻿using System.Text;
using CardLibrary;
using LanguageExt;
using LanguageExt.Common;
using War__easy_;
using static LanguageExt.Prelude;

Eff<Either<Error,GamePreparedData>> PrepareData()
{
    var rawData =
        from random in Eff(() => new Random())
        from trump in Eff(() => Enum.GetValues<Suite>().ToArr().GetRandom(random).Map((x) => new Trump(x)))
        from cards in Eff(() => Enum.GetValues<Suite>()
            .Map((suite) => Enum.GetValues<Rank>().Map((rank) => new Card(suite, rank)))
            .SelectMany(x => x).ToArr())
        from shuffledCards in Eff(() => cards.OrderBy(x => random.Next()).ToSeq())
        let players = shuffledCards.EvenSplit()
        let preparedData = trump.Bind(t => players.Map(b =>
                new GamePreparedData(
                    t,
                    new Player1Deck(b.Item1.Map(_ => new Player1Card(_))),
                    new Player2Deck(b.Item2.Map(_ => new Player2Card(_)))
                )
            ))

        select preparedData;
        //let preparedData = trump.IfRight(t => t)
        //select preparedData.ToEither(Error.New("cannot split deck evenly"));
    return rawData;
}

GameResultData PlayGame(GamePreparedData data)
{
    var warComparer = new WarCompare(data.trump);

    var roundResults = data.player1.deck
        .Zip(data.player2.deck, (player1, player2) => (player1, player2))
        .Map((_) => (players: (_.player1, _.player2), winner: warComparer.Compare(_.player1, _.player2).toWinner()))
        .Map(x => new GameResultRoundData(x.players.player1, x.players.player2, x.winner));
    var gameResult = roundResults.Aggregate(
        (player1Score: Seq<PlayerCard>(), player2Score: Seq<PlayerCard>()),
        (current, round) => round.winner switch
        {
            Winner.Player1 => (current.player1Score.Add(round.player1).Add(round.player2), current.player2Score),
            Winner.Player2 => (current.player1Score, current.player2Score.Add(round.player1).Add(round.player2)),
            _ => (current.player1Score.Add(round.player1), current.player2Score.Add(round.player2)),
        });
    var scoreResult = new GameResultScoreData(
        new Player1ScorePile(gameResult.player1Score),
        new Player2ScorePile(gameResult.player2Score),
        gameResult.player1Score.Count > gameResult.player2Score.Count ?
        Winner.Player1 : gameResult.player1Score.Count < gameResult.player2Score.Count ?
        Winner.Player2 : Winner.Tie);

    return new GameResultData(data.trump, roundResults, scoreResult);
}

// display proper result of the game
// display game cannot start message if no game was ran
string FormatResult(Either<Error, GameResultData> optionalData)
{
    var process =
        from data in optionalData
        let result = data.Apply(_ =>
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(_.trump.Display());
            stringBuilder.AppendJoin("\n", _.roundDatas.Map(round => round.Display()));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine(_.scoreData.Display());
            return stringBuilder;
        })
        select result.ToString();

    return process.IfLeft((e) => e.ToString());
}

Eff<Unit> PrintResult(string str)
{
    return Eff(() =>
    {
        Console.WriteLine(str);
        return Unit.Default;
    });
}


var process =
    from data in PrepareData()
    let gameResult = data.Map(PlayGame)
    select gameResult;

process
    .Map(FormatResult)
    .Bind(PrintResult)
    .Run()
    .Match(
    Succ: _ => { },
    Fail: error =>
    {
        Console.Error.WriteLine(error);
        Environment.Exit(1);
    });
