﻿using System;
namespace CardLibrary
{
	public enum CompareResult
	{
		Left,
		Equals,
		Right,
	}

	public interface ICompare<T>
	{
		CompareResult Compare(T a, T b);
    }
}

